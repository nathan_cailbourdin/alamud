# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import TurnOffEvent

class TurnOffAction(Action2):
    EVENT = TurnOffEvent
    ACTION = "turnoff"
    RESOLVE_OBJECT = "resolve_for_operate"
