# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import BrokeEvent

class BrokeAction(Action2):
    EVENT = BrokeEvent
    ACTION = "broke"
    RESOLVE_OBJECT = "resolve_for_use"
