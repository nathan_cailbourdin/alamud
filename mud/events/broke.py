# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class BrokeEvent(Event2):
    NAME = "broke"

    def perform(self):
        if not self.object.has_prop("brokable"):
            self.fail()
            return self.inform("broke.failed")
        self.inform("broke")
