# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class TurnOffEvent(Event2):
    NAME = "turnoff"

    def perform(self):
        if not self.object.has_prop("turnable"):
            self.fail()
            return self.inform("turnoff.failed")
        self.inform("turnoff")
