# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class ControlWithEvent(Event3):
    NAME = "control-with"

    def perform(self):
        if not (self.object.has_prop("controllablewith") and self.object2.has_prop("controller")):
            self.fail()
            return self.inform("control-with.failed")
        self.inform("control-with")
